Pakyow::App.routes do
  default do
    logger.info 'hello'

  data(:stats).view_default_post

  # render the stats
  view.scope(:stats).mutate(:post,with: data(:stats).for_default_post)


  view.partial(:'comments').scope(:comment).mutate(:list,with: data(:comment).for_default_post)

  # setup the form for a new object
  view.partial(:'new_comment').scope(:comment).bind({})


  end

  restful :comment, '/comments' do
    action :create do
      # create the comment
      data(:comment).create(params[:comment])

      # go back
      redirect :default
    end
  end
end
